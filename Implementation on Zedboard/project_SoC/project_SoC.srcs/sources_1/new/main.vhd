library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
USE work.types.all;
use work.sine_package.all;

 
entity main is
 port (
      clk           : in std_logic;
      o_uart_tx     : out  std_logic;
      i_uart_rx     : in  std_logic;
      LED0          : out std_logic;
      LED1         : out std_logic;
      LED2         : out std_logic;
      LED3         : out std_logic;
      LED4         : out std_logic;
      LED5         : out std_logic;
      LED6         : out std_logic;
      LED7         : out std_logic
      );
end main;
 
architecture behave of main is

    component uart_tx is
    generic (
      g_CLKS_PER_BIT : integer := 870   -- Needs to be set correctly
      );
    port (
      i_clk       : in  std_logic;
      i_tx_dv     : in  std_logic;
      i_tx_byte   : in  std_logic_vector(7 downto 0);
      o_tx_active : out std_logic;
      o_tx_serial : out std_logic;
      o_tx_done   : out std_logic
      );
  end component uart_tx;
 
  component uart_rx is
    generic (
      g_CLKS_PER_BIT : integer := 870   -- Needs to be set correctly
      );
    port (
      i_clk       : in  std_logic;
      i_rx_serial : in  std_logic;
      o_rx_dv     : out std_logic;
      o_rx_byte   : out std_logic_vector(7 downto 0)
      );
  end component uart_rx;
  
 component sine_wave
    port( sample_clock,sin_clock, reset, enable: in std_logic;
        wave_out: out sine_vector_type);
    end component;
  
  -- Test Bench uses a 100 MHz Clock
  -- Want to interface to 115200 baud UART
  -- 100000000 / 115200 = 870 Clocks Per Bit.
  constant c_CLKS_PER_BIT : integer := 870;
 
  constant c_BIT_PERIOD : time := 8680 ns;
   
  signal r_TX_DV    : std_logic                    := '0';
  signal r_TX_BYTE  : std_logic_vector(7 downto 0) := (others => '0');
  signal w_TX_DONE  : std_logic;
  signal w_RX_DV    : std_logic;
  signal w_RX_BYTE  : std_logic_vector(7 downto 0);
  signal sample_clk : std_logic :='0';
  signal sin_clk    : std_logic:='0';
  signal u          : std_logic_vector(7 downto 0);
  signal y          : std_logic_vector(21 downto 0);
  signal y_max      : std_logic_vector(21 downto 0) := (others => '0'); -- maximalwert 150 000
  signal r_perc     : std_logic_vector(7 downto 0);
  signal tx_enable  : std_logic := '1';
  signal reset_flag : std_logic := '1';
  signal reset_hand : std_logic := '1';
  signal s_uart_tx  : std_logic := '0';

  signal sin_speed  : unsigned(15 downto 0) := to_unsigned(200,16);--(others => '0');
  signal temp : std_logic := '0';
   
begin
 
  -- Instantiate UART transmitter
  UART_TX_INST : uart_tx
    generic map (
      g_CLKS_PER_BIT => c_CLKS_PER_BIT
      )
    port map (
      i_clk       => clk,
      i_tx_dv     => r_TX_DV,
      i_tx_byte   => r_TX_BYTE,
      o_tx_active => open,
      o_tx_serial => o_uart_tx,
      o_tx_done   => w_TX_DONE
      );

  -- Instantiate UART Receiver
  UART_RX_INST : uart_rx
    generic map (
      g_CLKS_PER_BIT => c_CLKS_PER_BIT
      )
    port map (
      i_clk       => clk,
      i_rx_serial => i_uart_rx,
      o_rx_dv     => w_RX_DV,
      o_rx_byte   => w_RX_BYTE
      );
      
     uut: sine_wave port map ( 
     sample_clock=>sample_clk,
      sin_clock=>sin_clk, 
      reset=>'1', 
      enable=>'1', 
      wave_out=>u );
    
    filter: entity work.fir
		port map(
			clk=>sample_clk,
			reset_n=>'1',
			data=>u,
			result=>y			
	);
   
   clkgen: process(clk)
    variable sample_count : unsigned(8 downto 0) := (others => '0');
    variable sin_count : unsigned(20 downto 0) := (others => '0');

   begin
    if rising_edge(clk) then
        sample_count := sample_count + 1;
        sin_count := sin_count + 1;
			if sample_count > 113 then -- 44KHz 22us = 100Mhz/227/2 == 113,6steps*2*100ns = 44Khz
			   sample_count := (others => '0');
			     sample_clk <= not sample_clk; 
			end if;
			
			if sin_count > sin_speed then --  
			   sin_count := (others => '0');
			     sin_clk <= not sin_clk; 
			end if;
    end if;
   end process;
   
   SEND: process(clk)
    variable count1: unsigned(24 downto 0) := (others => '0');
   begin
    if rising_edge(clk) then
        count1 := count1 + 1;
         r_TX_DV   <= '0';
			if count1 > 7000000 and w_TX_DONE = '0' and tx_enable = '1'  then
			   count1 := (others => '0');
			   
			     r_TX_BYTE <= r_perc;
			      r_TX_DV   <= '1';
			   
			end if;
    end if;
   end process;
   
    LED_0_5s: process(clk)
    variable count1: unsigned(30 downto 0) := (others => '0');
   begin
    if rising_edge(clk) then
        count1 := count1 + 1;
			if count1 > 100000000 then
			   count1 := (others => '0');
			   temp <= not temp;
			end if;
    end if;
   end process;
   LED0 <= temp;
   
   calc_max: process(sample_clk)
   begin
    if rising_edge(sample_clk) then
        if reset_flag = '1' then
            reset_hand <= '1';
            y_max <= (others => '0');
        else
            reset_hand <= '0';
            if signed(y) > signed(y_max) then
	           y_max <= y;
	           r_perc <=  y_max(18 downto 11);
	       end if;
    
        end if;
   end if;
   end process;
   
   receiveRX: process(clk,w_RX_DV)
    
   variable count1: unsigned(30 downto 0) := (others => '0');
   begin
   
    if rising_edge(clk) then
        if reset_hand = '1' then
            reset_flag <= '0';
        end if;
        if w_RX_DV = '1' then
            LED1 <= '0';
            LED2 <= '0';
            LED3 <= '0';
            LED4 <= '0';
            LED5 <= '0';
            LED6 <= '0';
              case w_RX_BYTE is
                when "00101011" =>  -- +
                    LED1 <= '1'; 
                    if sin_speed > 1 then
                      sin_speed <= sin_speed + 1;
                      reset_flag <= '1';
                    end if;
                when "00101101" =>  -- -
                    LED2 <= '1'; 
                    if sin_speed < 5000 then
                      sin_speed <= sin_speed - 1;
                      reset_flag <= '1';
                    end if;
                when "01100101" =>  -- e 
                    LED3 <= '1'; 
                    tx_enable <= '1';
                when "01100001" =>  -- a
                    LED4 <= '1'; 
                    tx_enable <= '0';
                    
                    
                when "00000000" =>   
                LED5 <= '1'; 
                when "01100110" =>   
                LED6 <= '1';
    
                when others => LED7 <= '1';
            end case;
        end if;
    end if;
   end process;
end behave;