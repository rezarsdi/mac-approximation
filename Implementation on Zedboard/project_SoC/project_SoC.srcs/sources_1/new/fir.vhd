LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
USE work.types.all;


ENTITY fir IS
	PORT(
			clk				:	IN		STD_LOGIC;                                  --system clock
			reset_n			:	IN		STD_LOGIC;                                  --active low asynchronous reset
			data			:	IN		STD_LOGIC_VECTOR(data_width-1 DOWNTO 0);    --data stream
			result			:	OUT	    STD_LOGIC_VECTOR((data_width + coeff_width + integer(ceil(log2(real(taps)))) - 1) DOWNTO 0));    --filtered result

END fir;

ARCHITECTURE behavior OF fir IS
	SIGNAL coeff_int : coefficient_array;
    type tap_line is array(taps downto 0) of std_logic_vector(data_width-1 downto 0);
    SIGNAL pipe : tap_line;
    

BEGIN
   
	PROCESS(clk, reset_n,coeff_int) --17 downto 0
		VARIABLE sum : SIGNED((data_width + coeff_width + integer(ceil(log2(real(taps)))) - 1) DOWNTO 0); --sum of products
	    VARIABLE temp : SIGNED((data_width + coeff_width -1 ) DOWNTO 0); --sum of products
	   
	BEGIN
	
		IF(reset_n = '0') THEN                                       --asynchronous reset
		
			coeff_int <= (OTHERS => (OTHERS => '0'));		         --clear internal coefficient registers
			result <= (OTHERS => '0');                               --clear result output				
		ELSIF(clk'EVENT AND clk = '1') THEN                          --not reset

			coeff_int <= coefficients;							
                                            
            FOR i IN taps DOWNTO 1 LOOP
                   pipe(i) <= pipe(i-1);                 
            END LOOP;
            
            pipe(0) <= data;
			result <=  STD_LOGIC_VECTOR(sum);			
			
        END IF;
        
        sum := (OTHERS => '0'); 
        
        FOR i IN 0 TO taps-1 LOOP -- sum = 22 bit
            temp := signed(coeff_int(i)) * signed(pipe(i));
			sum := sum + signed(coeff_int(i)) * signed(pipe(i));
			
       END LOOP;
            
	END PROCESS;
	
END behavior;