LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
use ieee.std_logic_signed.all;


PACKAGE types IS

	CONSTANT taps        : INTEGER := 57; --number of fir filter taps
	CONSTANT data_width  : INTEGER := 8; --width of data input including sign bit
	CONSTANT coeff_width : INTEGER := 8; --width of coefficients including sign bit
	
	TYPE coefficient_array IS ARRAY (0 TO taps-1) OF SIGNED(coeff_width-1 DOWNTO 0);  --array of all coefficients
	TYPE data_array IS ARRAY (0 TO taps-1) OF SIGNED(data_width-1 DOWNTO 0);                    --array of historic data values
	TYPE product_array IS ARRAY (0 TO taps-1) OF SIGNED((data_width + coeff_width)-1 DOWNTO 0); --array of coefficient * data products
	
	--2kHz Pass
	--4kHz Stop
	--44kHz sampling rate
	CONSTANT coefficients : coefficient_array := ( X"00", X"01", X"01", X"02", X"02", X"03", X"03", X"03", X"02", X"01", X"fe", X"fa", X"f6", X"f2", X"ed", X"ea", X"e9", X"eb", X"f0", X"f9", X"06", X"16", X"28", X"3b", X"4e", X"5f", X"6d", X"75", X"78", X"75", X"6d", X"5f", X"4e", X"3b", X"28", X"16", X"06", X"f9", X"f0", X"eb", X"e9", X"ea", X"ed", X"f2", X"f6", X"fa", X"fe", X"01", X"02", X"03", X"03", X"03", X"02", X"02", X"01", X"01", X"00");

END PACKAGE types;