#set_property PACKAGE_PIN L16 [get_ports clk]
#set_property IOSTANDARD LVCMOS33 [get_ports clk]
#create_clock -add -name sys_clk_pin -period 100.00 -waveform {0 4} [get_ports clk]


# ----------------------------------------------------------------------------
# JB Pmod - Bank 13
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN V10 [get_ports o_uart_tx];  # "JB3"
set_property PACKAGE_PIN W8 [get_ports i_uart_rx];  # "JB4"

set_property IOSTANDARD LVCMOS33 [get_ports o_uart_tx]
set_property IOSTANDARD LVCMOS33 [get_ports i_uart_rx]

set_property PACKAGE_PIN T22 [get_ports LED0];  # "LD0"
set_property PACKAGE_PIN T21 [get_ports LED1];  # "LD1"
set_property PACKAGE_PIN U22 [get_ports LED2];  # "LD2"
set_property PACKAGE_PIN U21 [get_ports LED3];  # "LD3"
set_property PACKAGE_PIN V22 [get_ports LED4];  # "LD4"
set_property PACKAGE_PIN W22 [get_ports LED5];  # "LD5"
set_property PACKAGE_PIN U19 [get_ports LED6];  # "LD6"
set_property PACKAGE_PIN U14 [get_ports LED7];  # "LD7"
set_property IOSTANDARD LVCMOS33 [get_ports LED0];
set_property IOSTANDARD LVCMOS33 [get_ports LED1];
set_property IOSTANDARD LVCMOS33 [get_ports LED2];
set_property IOSTANDARD LVCMOS33 [get_ports LED3];
set_property IOSTANDARD LVCMOS33 [get_ports LED4];
set_property IOSTANDARD LVCMOS33 [get_ports LED5];
set_property IOSTANDARD LVCMOS33 [get_ports LED6];
set_property IOSTANDARD LVCMOS33 [get_ports LED7];

# ----------------------------------------------------------------------------
# User LEDs - Bank 33
# ---------------------------------------------------------------------------- 
#set_property PACKAGE_PIN T22 [get_ports {LD0}];  # "LD0"
#set_property PACKAGE_PIN T21 [get_ports {LD1}];  # "LD1"
#set_property PACKAGE_PIN U22 [get_ports {LD2}];  # "LD2"
#set_property PACKAGE_PIN U21 [get_ports {LD3}];  # "LD3"
#set_property PACKAGE_PIN V22 [get_ports {LD4}];  # "LD4"
#set_property PACKAGE_PIN W22 [get_ports {LD5}];  # "LD5"
#set_property PACKAGE_PIN U19 [get_ports {LD6}];  # "LD6"
#set_property PACKAGE_PIN U14 [get_ports {LD7}];  # "LD7"
# Note that the bank voltage for IO Bank 33 is fixed to 3.3V on ZedBoard. 
#set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];

# Note that the bank voltage for IO Bank 13 is fixed to 3.3V on ZedBoard. 

#set_property PACKAGE_PIN W12 [get_ports o_uart_tx]
#set_property PACKAGE_PIN W11 [get_ports i_uart_rx]
#set_property PACKAGE_PIN V12 [get_ports {JB7}];  # "JB7"
#set_property PACKAGE_PIN W10 [get_ports {JB8}];  # "JB8"
#set_property PACKAGE_PIN V9 [get_ports {JB9}];  # "JB9"
#set_property PACKAGE_PIN V8 [get_ports {JB10}];  # "JB10"
#set_property PACKAGE_PIN Y9 [get_ports clk];  # "GCLK"

set_property PACKAGE_PIN Y9 [get_ports clk];  # "GCLK"
set_property IOSTANDARD LVCMOS33 [get_ports clk];
#create_clock -period 100.000 -name clk -waveform {0.000 50.000} [get_ports clk]

