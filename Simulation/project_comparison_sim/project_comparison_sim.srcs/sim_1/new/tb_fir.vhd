library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.math_real.all;
USE work.types.all;
USE work.sine_package.all;

entity tb_fir is

end entity tb_fir;

architecture rtl of tb_fir is
    signal reset:std_ulogic:='0';
    signal u: std_logic_vector(data_width-1 downto 0) := (others=>'0');
    signal y: signed(result_width-1 DOWNTO 0);
    signal temp: signed(result_width-1 DOWNTO 0);
    signal trig:std_logic;
    signal output: STD_LOGIC_VECTOR(data_width-1 downto 0); 
    signal clk:std_ulogic:='0';
    signal sample_clk:std_ulogic:='0';
    signal sin_clk:std_ulogic:='0';
    signal enable:std_ulogic:='1';
    signal count:unsigned(8 downto 0);
    signal pwrUpCnt:unsigned(3 downto 0):=(others=>'0');
    signal count_sin :unsigned(15 downto 0) := to_unsigned(2000,16);
     
    --BEGINN: LOA: lower-part OR adder
    signal y_loa: SIGNED(result_width-1 DOWNTO 0);
    signal y_wal_loa: SIGNED(result_width-1 DOWNTO 0);
    --END: LOA: lower-part OR adder	
    --BEGINN: simple shift adder
    signal y_simple_shift: SIGNED(result_width-1 DOWNTO 0);
    signal y_wal_simple_shift: SIGNED(result_width-1 DOWNTO 0);

    --END: simple shift adder
    --BEGINN: segmented adder
    signal y_segmented: SIGNED(result_width-1 DOWNTO 0);
    signal y_wal_segmented: SIGNED(result_width-1 DOWNTO 0);
    --END: segmented adder	
    
    component sine_wave
    port( sample_clock,sin_clock, reset, enable: in std_logic;
        wave_out: out sine_vector_type);
    end component;
  
begin
    
    filter: entity work.fir
    port map(
        clk=>sample_clk,
        reset_n=>reset,
        data=>u,
        coefficients=>coefficients,
        result=>y,
        --BEGINN: LOA: lower-part OR adder
        loa_result=>y_loa,
        loa_wal_result=>y_wal_loa,
        --END: LOA: lower-part OR adder	
        --BEGINN: simple shift adder
        simple_shift_result=>y_simple_shift,
        simple_shift_wal_result=>y_wal_simple_shift,
        --END: simple shift adder
        --BEGINN: segmented adder
        segmented_result=>y_segmented,
        segmented_wal_result=>y_wal_segmented
        --END: segmented adder
    );
 
 clk<=not clk after 10 ns;
 --period = 1/44kHz = 22.73us => 11us
 sample_clk<=not sample_clk after 11us;
 
 process(clk) is begin
  if rising_edge(clk) then
   if pwrUpCnt<10 then 
        pwrUpCnt<=pwrUpCnt+1; 
        reset<='0';
   else 
        reset<='1';
   end if;
  end if;
 end process;

 
 process(reset,sample_clk) is begin
  if reset='0' then count<=(others =>'0');
  elsif rising_edge(sample_clk) then
   if count<50 then count<=count+1; end if;
  end if;
 end process;
 
 uut: sine_wave 
    port map( 
        sample_clock=>sample_clk, 
        sin_clock=>sin_clk, 
        reset=>reset, 
        enable=>enable, 
        wave_out=>u
    );
 
 --sinclk = Period /256 
 -- (1/1kHz)/256 = 3.9us
 -- (1/5kHz)/256 = 781.3ns
SIN : process(clk)
    variable count1: unsigned(16 downto 0) := (others => '0');
    begin
        if rising_edge(clk) then
            count1 := count1 + 1;
            if count1 > count_sin then
                sin_clk <= not sin_clk;
                count1 := (others => '0');
            end if;
         end if; 
    end process;   -- 15ms periode = /256 = 0,00005859375 580us = /2 =290us
 
SIN_Speed: process(clk)
    variable count1: unsigned(20 downto 0) := (others => '0');
    begin
        if rising_edge(clk) then
            count1 := count1 + 1;
            if count_sin > 2  AND count1 > 5000  then
                count_sin <= count_sin-1;
                count1 := (others => '0');
            end if;
        end if; --15us 
 end process;

--impulse response
--u <= x"ef" when count=20 else (others=>'0');




end architecture rtl;
