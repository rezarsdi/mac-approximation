LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
USE work.types.all;
USE work.Wallace8;

ENTITY fir IS
	PORT(
			clk				:	IN		STD_LOGIC;                                  --system clock
			reset_n			:	IN		STD_LOGIC;                                  --active low asynchronous reset
			data			:	IN		STD_LOGIC_VECTOR(7 DOWNTO 0);               --input data
			coefficients	:	IN		coefficient_array;                          --coefficient array
			result			:	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0'); -- result with no approximation
			
			wal_result	    :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');   
           
			--BEGINN: LOA: lower-part OR adder
            loa_result	    :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');   
            loa_wal_result	:	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');    
 
            --END: LOA: lower-part OR adder	
            --BEGINN: simple shift adder
            simple_shift_result	        :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');
            simple_shift_wal_result	    :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');
            --END: simple shift adder
            --BEGINN: segmented adder
            segmented_result	        :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0');
            segmented_wal_result	    :	OUT	    SIGNED(result_width-1 DOWNTO 0) := (OTHERS => '0'));
            --END: segmented adder
END fir;

ARCHITECTURE behavior OF fir IS
	SIGNAL coeff_int : coefficient_array := (others=>(others=>'0'));
    type tap_line is array(taps-1 downto 0) of signed(7 downto 0);
    SIGNAL pipe : tap_line := (others=>(others=>'0'));
    type mul_line is array(taps-1 downto 0) of SIGNED(15 downto 0);
    SIGNAL mul_out : mul_line := (others=>(others=>'0'));   
    
    function loa (
        ADD1 : in signed(result_width-1 downto 0);
        ADD2 : in signed(result_width-1 downto 0))
        return signed is
            variable precise_result : signed(loa_precise_width-1 downto 0);
            variable approx_result : signed(loa_approx_width-1 downto 0);
            variable result : signed(result_width-1 downto 0);
        begin
            precise_result := ADD1(result_width-1 downto loa_approx_width) + ADD2(result_width-1 downto loa_approx_width);
            approx_result := ADD1(loa_approx_width-1 downto 0) or ADD2(loa_approx_width-1 downto 0);
            
            result(result_width-1 downto loa_approx_width) := precise_result;
            result(loa_approx_width-1 downto 0) := approx_result;
            return result;

    end function;
    
    function simple_shift (
        ADD1 : in signed(result_width-1 downto 0);
        ADD2 : in signed(result_width-1 downto 0))
        return signed is
            variable shift_result : signed(simple_shift_width-1 downto 0);
            variable result : signed(result_width-1 downto 0);
        begin
            shift_result := ADD1(result_width-1 downto result_width-simple_shift_width) + ADD2(result_width-1 downto result_width-simple_shift_width);
            
            result(result_width-1 downto result_width-simple_shift_width) := shift_result;
            result(result_width-simple_shift_width-1 downto 0) := (others=>'1');
            return result;

    end function;
    
    function segmented_adder (
        ADD1 : in signed(result_width-1 downto 0);
        ADD2 : in signed(result_width-1 downto 0))
        return signed is
            type segmented_wal_result is array(segment_count-1 downto 0) of signed(segment_size-1 downto 0);
            variable segmented_wal_results : segmented_wal_result;
            variable last_result : signed(result_width-((segment_count-1)*segment_size)-1 downto 0);
            variable result : signed(result_width-1 downto 0);
        begin            
            segmented_adder_loop : for i in segment_count-1 downto 1 loop
                segmented_wal_results(i) := ADD1((result_width-((segment_count-1-i)*segment_size))-1 downto (result_width-((segment_count-i)*segment_size))) + ADD2((result_width-((segment_count-1-i)*segment_size))-1 downto (result_width-((segment_count-i)*segment_size)));
                result((result_width-((segment_count-1-i)*segment_size))-1 downto (result_width-((segment_count-i)*segment_size))) := segmented_wal_results(i);
            end loop;
            -- the last (lowest order) segment might not have a full segment_size width, therefor we need to handle it separatly
            last_result := ADD1((result_width-((segment_count-1)*segment_size))-1 downto 0) + ADD2((result_width-((segment_count-1)*segment_size))-1 downto 0);
            result((result_width-((segment_count-1)*segment_size))-1 downto 0) := last_result;
            
            return result;

    end function;
    
    
    component Wallace8 is
	port (
		A : IN SIGNED(7 downto 0);
		B : IN SIGNED(7 downto 0);
		P : OUT SIGNED(15 downto 0)
	);
    end component;
	
BEGIN

    gen: for i in 0 to taps-1 generate
        mul: Wallace8 port map (A => pipe(i), B => coefficients(i), P => mul_out(i));        
    end generate;       
    
	PROCESS(clk, reset_n, coeff_int, mul_out,pipe)
		VARIABLE sum : SIGNED(result_width-1 DOWNTO 0); --sum of products
		VARIABLE mpro : SIGNED(15 DOWNTO 0); --multiplication product
        VARIABLE mpro_wallace : SIGNED(15 DOWNTO 0); --multiplication product
        
        VARIABLE wal_sum : SIGNED(result_width-1 DOWNTO 0); --combinition of exact and approximate subadder

	    --BEGINN: LOA: lower-part OR adder
        VARIABLE loa_sum : SIGNED(result_width-1 DOWNTO 0); --combinition of exact and approximate subadder
        VARIABLE loa_wal_sum : SIGNED(result_width-1 DOWNTO 0); --combinition of exact and approximate subadder
        --END: LOA: lower-part OR adder	
        --BEGINN: simple shift adder
        VARIABLE simple_shift_sum : SIGNED(result_width-1 DOWNTO 0); --only calculate the highest bits
        VARIABLE simple_shift_wal_sum : SIGNED(result_width-1 DOWNTO 0); --only calculate the highest bits
        --END: simple shift adder
        --BEGINN: segmented adder
        VARIABLE segmented_sum : SIGNED(result_width-1 DOWNTO 0); --split into sub adders
        VARIABLE segmented_wal_sum : SIGNED(result_width-1 DOWNTO 0); --split into sub adders
        --END: segmented adder
	BEGIN
	
		IF(reset_n = '0') THEN                                       --asynchronous reset
			coeff_int <= (OTHERS => (OTHERS => '0'));		         --clear internal coefficient registers
			result <= (OTHERS => '0');                               --clear result output
            --BEGINN: LOA: lower-part OR adder
            loa_wal_result <= (OTHERS => '0');                                  --clear result output
            loa_result <= (OTHERS => '0');                                  --clear 
            wal_result <= (OTHERS => '0');                                  --clear result output

            --END: LOA: lower-part OR adder	
            --BEGINN: LOA: simple shift adder
			simple_shift_wal_result <= (OTHERS => '0');                                  --clear result output
            simple_shift_result <= (OTHERS => '0');                                  --clear result output
            --END: simple shift adder	
            --BEGINN: segmented adder
			segmented_wal_result <= (OTHERS => '0');                                  --clear result output
            segmented_result <= (OTHERS => '0');                                  --clear result output
            --END: segmented adder				
		ELSIF(clk'EVENT AND clk = '1') THEN                          --not reset
			coeff_int <= coefficients;							
                          
            FOR i IN taps-1 DOWNTO 1 LOOP
                   pipe(i) <= pipe(i-1);                 
            END LOOP;
            
            pipe(0) <= signed(data);
			
			result <= sum(result_width-1 downto 0);	 
			
			wal_result <= wal_sum(result_width-1 downto 0);
            
            --BEGINN: LOA: lower-part OR adder                         
            loa_wal_result <= loa_wal_sum(result_width-1 downto 0);
            loa_result <= loa_sum(result_width-1 downto 0);
            
            --END: LOA: lower-part OR adder	
            --BEGINN: simple shift adder                         
			simple_shift_wal_result <= simple_shift_wal_sum(result_width-1 downto 0);
            simple_shift_result <= simple_shift_sum(result_width-1 downto 0);
            --END: simple shift adder	
            --BEGINN: segmented adder                         
			segmented_wal_result <= segmented_wal_sum(result_width-1 downto 0);
            segmented_result <= segmented_sum(result_width-1 downto 0);
            --END: segmented adder				
			
        END IF;
         
        sum := (OTHERS => '0');
        
        wal_sum := (OTHERS => '0');

        --BEGINN: LOA: lower-part OR adder 
        loa_wal_sum := (OTHERS => '0');
        loa_sum := (OTHERS => '0');
        --END: LOA: lower-part OR adder	
        --BEGINN: simple shift adder
        simple_shift_wal_sum := (OTHERS => '0');
        simple_shift_sum := (OTHERS => '0');
        --END: simple shift adder
        --BEGINN: segmented adder
        segmented_sum := (OTHERS => '0');
        segmented_wal_sum := (OTHERS => '0');
        --END: segmented adder
                
        FOR i IN 0 TO taps-1 LOOP
            
            mpro_wallace := SIGNED(mul_out(i));
            --mpro := signed(coeff_int(i)) * signed(pipe(i));
            
            wal_sum := wal_sum + resize(mpro_wallace,22);

            --BEGINN: LOA: lower-part OR adder
            --STRUGGLE: lower part bit width should depend on the products(i) bit width
            loa_wal_sum := loa(loa_wal_sum, resize(mpro_wallace,22));
            loa_sum := loa(loa_sum, resize(signed(coeff_int(i)) * signed(pipe(i)),22));
            --END: LOA: lower-part OR adder
            --BEGINN: simple shift adder
            simple_shift_wal_sum := simple_shift(simple_shift_wal_sum, resize(mpro_wallace,22));
            simple_shift_sum := simple_shift(simple_shift_sum, resize(signed(coeff_int(i)) * signed(pipe(i)),22));
            --END: simple adder
            --BEGINN: segmented adder
            segmented_wal_sum := segmented_adder(segmented_wal_sum, resize(mpro_wallace,22));
            segmented_sum := segmented_adder(segmented_sum, resize(signed(coeff_int(i)) * signed(pipe(i)),22));
            --END: segmented adder
            -- THIS IS THE EXACT (REFERENCE) RESULT
			sum := sum + signed(coeff_int(i)) * signed(pipe(i));
			
       END LOOP;
            
	END PROCESS;
	
END behavior;