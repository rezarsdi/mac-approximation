% format short g
% 
% N   = 30;        % FIR filter order
% Fp  = 1e6;       % 1MHz passband-edge frequency
% Fs  = 25e6;      % 25MHz sampling frequency
% Rp  = 0.01;      % Corresponds to 0.01 dB peak-to-peak ripple
% Rst = -46.052;      % Corresponds to 80 dB stopband attenuation
% 
% b = firceqrip(N,Fp/(Fs/2),[Rp Rst],'passedge');


% All frequency values are in kHz.
Fs = 44;  % Sampling Frequency

Fpass = 2;               % Passband Frequency
Fstop = 4;               % Stopband Frequency
Dpass = 0.057501127785;  % Passband Ripple
Dstop = 0.0001;          % Stopband Attenuation
dens  = 20;              % Density Factor

% Calculate the order from the parameters using FIRPMORD.
[N, Fo, Ao, W] = firpmord([Fpass, Fstop]/(Fs/2), [1 0], [Dpass, Dstop]);

% Calculate the coefficients using the FIRPM function.
b  = firpm(N, Fo, Ao, W, {dens});
fvtool(b,'Fs',Fs,'Color','White') % Visualize filter

h = dfilt.dffir(b);
h.Arithmetic = 'fixed';
h.CoeffWordLength = 8;
h.InputWordLength = 8;
h.InputFracLength = 5;

fvtool(h, 'Color', 'white');

impz(h)

coewrite(h, 16, 'coefile');


%simulation in vivado 
% 2.    When the XSIM simulation window appears, enter these commands in the tcl console:
%  
% open_vcd
% log_vcd [get_object /<toplevel_testbench/uut/*>]
% run *ns
% close_vcd



