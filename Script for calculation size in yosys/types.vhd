LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
use ieee.std_logic_signed.all;


PACKAGE types IS

	CONSTANT taps        : INTEGER := 57; 
	CONSTANT data_width  : INTEGER := 8; 
	CONSTANT coeff_width : INTEGER := 8; 
	CONSTANT result_width : INTEGER := (data_width + coeff_width + integer(ceil(log2(real(taps))))); --full with of result after accumulation
    --BEGINN: LOA: lower-part OR adder
	CONSTANT loa_precise_width : INTEGER := 18; --width of the precise (high-order) part of the lower part OR adder
	CONSTANT loa_approx_width : INTEGER := result_width - loa_precise_width; --width of the approximate (low-order) part of the lower part OR adder
    --END: LOA: lower-part OR adder	
    --BEGINN: simple shift adde
    CONSTANT simple_shift_width : INTEGER := 20; --width of the precise result. The amount of high order bits used for the precise calculation
    --END: simple shift adder
    --BEGINN: segmented adder
    CONSTANT segment_count : INTEGER := 5; --amount of segements. e.g. 20 bit adder with segment_count of 5 gives 5 4-bit subadders
    CONSTANT segment_size : INTEGER := result_width / segment_count;
    --END: segmented adder

	TYPE coefficient_array IS ARRAY (0 TO taps-1) OF SIGNED(coeff_width-1 DOWNTO 0);  --array of all coefficients
	TYPE data_array IS ARRAY (0 TO taps-1) OF SIGNED(data_width-1 DOWNTO 0);                    --array of historic data values
	TYPE product_array IS ARRAY (0 TO taps-1) OF SIGNED((data_width + coeff_width)-1 DOWNTO 0); --array of coefficient * data products
	
	
	--CONSTANT coefficients : coefficient_array := ( X"01", X"fa", X"f6", X"01", X"20", X"43", X"54", X"43", X"20", X"01", X"f6", X"fa", X"01");
    --CONSTANT coefficients : coefficient_array := ( X"01", X"fa");
 
    --2kHz Pass
    --4kHz Stop
    --44kHz sampling rate
    CONSTANT coefficients : coefficient_array := ( X"00", X"01", X"01", X"02", X"02", X"03", X"03", X"03", X"02", X"01", X"fe", X"fa", X"f6", X"f2", X"ed", X"ea", X"e9", X"eb", X"f0", X"f9", X"06", X"16", X"28", X"3b", X"4e", X"5f", X"6d", X"75", X"78", X"75", X"6d", X"5f", X"4e", X"3b", X"28", X"16", X"06", X"f9", X"f0", X"eb", X"e9", X"ea", X"ed", X"f2", X"f6", X"fa", X"fe", X"01", X"02", X"03", X"03", X"03", X"02", X"02", X"01", X"01", X"00");

END PACKAGE types;