from Verilog_VCD import parse_vcd
import collections 
import bisect 
from collections import OrderedDict


def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val 
    
vcd = parse_vcd("/Users/dominikdallinger/git/soc-project/Error Simulation/script/dump.vcd")

u = vcd[list(vcd.keys())[0]]["tv"]
y_dict = {}
y = vcd[list(vcd.keys())[1]]["tv"]
y_loa = vcd[list(vcd.keys())[2]]["tv"]
y_wal_loa = vcd[list(vcd.keys())[3]]["tv"]
y_simple_shift = vcd[list(vcd.keys())[4]]["tv"]
y_wal_simple_shift = vcd[list(vcd.keys())[5]]["tv"]

for values in y:
    timestamp = values[0]
    value = twos_comp(int(values[1],2),22)
    y_dict[timestamp] = value
    #print(value)

y_dict = {key:value for key, value in sorted(y_dict.items(), key=lambda item: int(item[0]))}


## ---------------- LOA ohne Wallace ----------------- ##
i = 0
percentage = 0
for values in y_loa:
    timestamp = values[0]
    value = twos_comp(int(values[1],2),22)
    nn  = y_dict[timestamp] if timestamp in y_dict else y_dict[min(y_dict.keys(), key=lambda k: abs(k-timestamp))]
    if value == 0 or nn == 0:
        percentage += 0
    else:
        percentage += abs(1 - abs(value/nn))
    i += 1
print("loa error = : " + str(percentage/i))

## ---------------- LOA mit Wallace ----------------- ##
i = 0
percentage = 0
for values in y_wal_loa:
    timestamp = values[0]
    value = twos_comp(int(values[1],2),22)
    nn  = y_dict[timestamp] if timestamp in y_dict else y_dict[min(y_dict.keys(), key=lambda k: abs(k-timestamp))]
    if value == 0 or nn == 0:
        percentage += 0
    else:
        percentage += abs(1 - abs(value/nn))
    i += 1
print("wal loa error = : " + str(percentage/i))

## ---------------- y_simple_shift ohne Wallace ----------------- ##
i = 0
percentage = 0
for values in y_simple_shift:
    timestamp = values[0]
    value = twos_comp(int(values[1],2),22)
    nn  = y_dict[timestamp] if timestamp in y_dict else y_dict[min(y_dict.keys(), key=lambda k: abs(k-timestamp))]
    if value == 0 or nn == 0:
        percentage += 0
    else:
        percentage += abs(1 - abs(value/nn))
    i += 1
print("simple shift error = : " + str(percentage/i))

## ---------------- y_simple_shift mit Wallace ----------------- ##
i = 0
percentage = 0
for values in y_wal_simple_shift:
    timestamp = values[0]
    value = twos_comp(int(values[1],2),22)
    nn  = y_dict[timestamp] if timestamp in y_dict else y_dict[min(y_dict.keys(), key=lambda k: abs(k-timestamp))]
    if value == 0 or nn == 0:
        percentage += 0
    else:
        percentage += abs(1 - abs(value/nn))
    i += 1
print("wal simple shift error = : " + str(percentage/i))