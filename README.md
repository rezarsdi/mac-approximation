**I. INTRODUCTION**

In this project a FIR Filter is used to show the advantages and disadvantages of
different approximation techniques.
To implement the multiplier, the Wallace algorithm is chosen. For the adder 
approximation Lower-Part OR Adder, Simple Shift Adder and Segmented Adder is 
used.

**II. FIR FILTER- FINITE IMPULSE RESPONSE FILTER**

The filter is designed in Matlab, using the built in the digital filter designer
with an equiripple design method and a lowpass response type.

**III. APPROXIMATE ACCUMULATION**

1.  Lower-Part OR Adder

The ”Lower-Part OR Adder” (LOA) is an approximate adder algorithm with a fixed 
maximum error rate.


2.  Simple Shift Adder

The Simple Shift Adder algorithm is derived from the LOA algorithm. We have seen 
that we can decrease the amount of hardware resources even further when used in 
an accumulative manner.

**IV. APPROXIMATE MULTIPLIER**

To implement the multiplier, the Wallace-Tree algorithm is chosen. Despite complexity
of this algorithm, the accuracy and the high speed of the computation convinced 
us to use this well-known algorithm.

**V. COMPARISON**

Comparison between Exact and approximate FIR filter implementations.

**VI. CONCLUSION**

We implemented the following modules in our design:
* Hardware DSP slice
* Reference Multiplier
* WallaceTree Multiplier
* Reference Accumulator
* Lower-part OR Adder
* SimpleShift Adder


We have shown that approximate computation can have positive impact in the need 
of logic cells when a less accurate result is acceptable.